<?php
session_start();
if(!file_exists('data/santa.db')) {
	$db = new PDO('sqlite:data/santa.db');
	$db->query('CREATE TABLE IF NOT EXISTS `santa` (
	  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
	  `name` TEXT NOT NULL,
	  `email` TEXT NOT NULL,
	  `preference` TEXT NOT NULL);');
}
else {
	$db = new PDO('sqlite:data/santa.db');
}
echo '<!doctype html>
	<html lang="fr">
	<head>
		<title>Santa</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/sofia" type="text/css"/> 
		<link rel="stylesheet" media="screen" href="assets/style.css" type="text/css"/>
		<style>label{display:block;}</style>
	</head>
	<body>
	<main>
	<h1>Secret Santa</h1>';
if(isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'admin':
			if((isset($_SESSION['login']) || !empty($_SESSION['login'])) AND $_SESSION['login'] == true) {
				if(isset($_GET['del'])) {
					$query = $db->prepare('DELETE FROM santa WHERE id=?');
					$query->execute(array((int)$_GET['del']));
					echo '<meta http-equiv="refresh" content="0; url=santa.php?do=admin">';
					
				}
				echo '<table><tr><th></th><th>Nom</th><th>Courriel</th><th>Préférences</th></tr>';
				foreach($db->query('SELECT * FROM santa') as $santa) {
					echo '<tr><td><a href="santa.php?do=admin&del='.$santa['id'].'">Supprimer</a></td><td>'.$santa['name'].'</td><td>'.$santa['email'].'</td><td>'.$santa['preference'].'</td></tr>';
				}
				echo '</table>';
				echo '<a href="?do=send">Envoyer les lettres aux Pères Noël</a>';
			}
			else {
				echo '<meta http-equiv="refresh" content="0; url=index.php?do=admin">';
			}
		break;
		case 'send':
			if((isset($_SESSION['login']) || !empty($_SESSION['login'])) AND $_SESSION['login'] == true) {
				
				$list = $db->query('SELECT * FROM santa')->fetchAll();
				$destinataire = $db->query('SELECT * FROM santa ORDER BY RANDOM()')->fetchAll();
				$correspondance = array();
				$i = 0;
				while($i <= (count($list)-1)) {
					$correspondance[$i] = array (
						'from_name' 	=> $list[$i]['name'],
						'from_email'	=> $list[$i]['email'],
						'to_name'		=> $destinataire[$i]['name'],
						'to_preference' => $destinataire[$i]['preference'],
					);
					$i++;
				}
				echo '<table><tr><th>Santa</th><th>Santa courriel</th><th>Bénéficiaire</th><th>Préférences</th></tr>';
				foreach($correspondance as $santa) {
					echo '<tr><td>'.$santa['from_name'].'</td><td>'.$santa['from_email'].'</td><td>'.$santa['to_name'].'</td><td>'.$santa['to_preference'].'</td></tr>';
					$mail = str_replace(array('%name%', '%preference%'), array($santa['to_name'], $santa['to_preference']), 'Salut c‘est pour le secret Santa. Ton protégé⋅e est <b>%name%</b>. Voici ses préférences : « %preference% ». Tu devras lui trouver un cadeau d‘une valeur de X€. À bientôt !');
					//mail($santa['from_email'], 'Secret Santa', $mail, array('From' => 'webmaster@example.com','Reply-To' => 'webmaster@example.com','X-Mailer' => 'PHP/' . phpversion()));
				}
				echo '</table>';
			}
		break;
	}
}
else {
	if(isset($_POST['email'])) {
		$prepare = $db->prepare('INSERT INTO santa(name, email, preference) VALUES(?,?,?);');
		$prepare->execute(array($_POST['name'], $_POST['email'], $_POST['preference']));
		
		echo '<p>Merci de ta participation. Tu recevras bientôt un courriel pour connaitre le nom de la personne à qui tu feras un cadeau. Garde le secret 😉</p>';
	}
	else {
	
	echo '<p>Bienvenue au Secret Santa ! La machine va attribuer à chacun des participants un destinataire auquel tu pourras faire un cadeau d‘une valeur de X €. Tu recevras un courriel avec les informations nécessaires. Si tu veux participer, inscris-toi, le tirage se fera bientôt ! <a href="https://fr.wikipedia.org/wiki/Secret_Santa">Découvrir le principe</a>.</p>
		<form method="post">
		<label for="name">Ton nom <input type="text" id="name" name="name" required></label>
		<label for="email">Ton courriel <input type="email" id="email" name="email" required></label>
		<label for="preference">C‘est le moment de rentrer les informations complémentaires. As tu des préférences particulières ? (Tu ne bois pas d‘alcool, tu es végane, etc). Éventuellement une adresse postale si tu fais le Santa à distance.</label>
		<textarea id="preference" name="preference"></textarea>
		<input type="submit">
		</form>';
	}	
}
echo'<footer><p><a href="?do=admin">Admin</a> <a href="santa.php">Secret Santa</a> <a href="index.php">Calendrier</a></p></footer></body>
	</html>';
