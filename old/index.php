<?php
$semaine = [
1 => 'Lundi',
2 => 'Mardi',
3 => 'Mercredi',
4 => 'Jeudi',
5 => 'Vendredi',
6 => 'Samedi',
7 => 'Dimanche'];
session_start();
define('DEV', false);
if(!isset($_GET['do'])) {$_GET['do'] = null;}
function show_header() {
return '<!doctype html>
<html lang="fr">
<head>
<title>Calendrier de l’avent</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/sofia" type="text/css"/> 
<link rel="stylesheet" media="screen" href="assets/style.css" type="text/css"/> 
<link rel="alternate" type="application/rss+xml" href="?do=rss" />
</head>
<body>
<main>
<h1>Calendrier de l’avent</h1>';
}
function show_footer() {
	$html = '</main><footer><p>';
	$html .= (isset($_GET['do'])) ? '<a href="?">Voir le calendrier</a> ' : '';
	$html .= ($_GET['do'] != 'admin') ? '<a href="?do=admin">Administration</a> ' : '';
	$html .='<a href="?do=rss">RSS</a> <a href="?do=about">À propos</a> <a href="santa.php">Secret Santa</a>';
	$html .= ((isset($_SESSION['login']) || !empty($_SESSION['login'])) AND $_SESSION['login'] == true) ? ' <a href="?do=logout">Déconnexion</a>' : '';
	$html .= '</footer>
</body>
</html>';
	return $html;
}
function redirect($url) {
	@header('Location: '.$url);
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}
function write_config($data) {
	file_put_contents('data/config.php', '<?php /* '.json_encode($data).' */ ?>');
}
function read_config() {
	$data = file_get_contents('data/config.php');
	$data = str_replace('<?php /* ', '', $data);
	$data = str_replace(' */ ?>', '', $data);
	$data = json_decode($data, true);
	return $data;
}
######################################
if(!file_exists('data')) {
	mkdir('data/');
	mkdir('data/upload/');
}
if(!file_exists('data/data.json')) {
	$gifts = array (
		1  => '',
		2  => '',
		3  => '',
		4  => '',
		5  => '',
		6  => '',
		7  => '',
		8  => '',
		9  => '',
		10 => '',
		11 => '',
		12 => '',
		13 => '',
		14 => '',
		15 => '',
		16 => '',
		17 => '',
		18 => '',
		19 => '',
		20 => '',
		21 => '',
		22 => '',
		23 => '',
		24 => '',
		25 => ''
	);
	file_put_contents('data/data.json', json_encode($gifts));
}
if(!file_exists('data/config.php')) {
	write_config(array('pwd'=>password_hash('demo', PASSWORD_DEFAULT)));

}
##########################
$config = read_config();
if(isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'login':
			if(isset($_POST['pwd']) AND (!isset($_SESSION['login']) || empty($_SESSION['login']))) {
				if(password_verify($_POST['pwd'], $config['pwd'])) {
					echo "Bon mot de passe";
					$_SESSION['login'] = true;
					redirect('?do=admin');
				}
				else {
					echo "Mauvais mot de passe";
					$_SESSION['login'] = false;
				}
			}
			else {
				echo show_header();
				echo '<form method="post" action="?do=login">'.PHP_EOL;
				echo '<label for="pwd">Votre mot de passe : <input type="password" name="pwd" id="pwd"></label>'.PHP_EOL;
				echo '<input type="submit"/>'.PHP_EOL;
				echo '</form>';
				echo show_footer();
			}
			
		break;
		case 'logout':
			if(isset($_SESSION['login'])) {
				unset($_SESSION['login']);
				session_destroy();
				redirect('index.php');
			}
		break;
		case 'about':
			echo show_header();
			echo '<p>Un petit logiciel minimaliste pour faire un calendrier de l’avent maison. Le <a href="https://framagit.org/qwertygc/calendrier-avent/">code source</a> est disponible, et les icônes viennent d’<a href="https://openmoji.org">Openmoji</a>. Joyeuses fêtes.</p>';
			echo show_footer();
		break;
		case 'admin':
			if((isset($_SESSION['login']) || !empty($_SESSION['login'])) AND $_SESSION['login'] == true) {
				echo show_header();
				echo '<form method="post" action="?do=admin">'.PHP_EOL;
				$gifts = json_decode(file_get_contents('data/data.json'), true);
				foreach ($gifts as $gift=>$link) {
					echo '<fieldset>
							<legend>Jour #'.$gift.' ('.$semaine[date('N', mktime(0, 0, 0, 12, (int)$gift, date("Y")))].')</legend>
							<label for="day_'.$gift.'_link">Lien '.($link != '' ? '<a href="'.$link.'" style="text-decoration:none;" target="_blank">⬈</a>':'').'
								<input type="url" name="day_'.$gift.'_link" value="'.$link.'" />
							</label>
						</fieldset>'.PHP_EOL;
				}
				echo '<input type="submit" name="submit" value="Envoyer"/>'.PHP_EOL;
				echo '</form>'.PHP_EOL;
				echo '<textarea style="width:100%;height:25rem;">';
				foreach ($gifts as $gift=>$link) {
					echo 'Jour #'.$gift.' : '.$link.PHP_EOL;
				}
				echo '</textarea>';
				if(isset($_POST['submit'])) {
					$sack = array();
					foreach ($gifts as $gift=>$link) {
						$sack[$gift] = $_POST['day_'.$gift.'_link'];
					}
					//print_r($sack);
					file_put_contents('data/data.json', json_encode($sack));
					echo 'Liens enregistrés';
					redirect('?do=admin');
				}
				echo '<form method="post" action="?do=admin">'.PHP_EOL;
				echo '<label for="pwd">Changer le mot de passe : <input type="password" name="pwd" id="pwd"></label>'.PHP_EOL;
				echo '<input type="submit"/>'.PHP_EOL;
				echo '</form>';
				if(isset($_POST['pwd'])) {
					write_config(array('pwd'=>password_hash($_POST['pwd'], PASSWORD_DEFAULT)));
					echo 'Mot de passe changé !';
				}
			echo show_footer();
		}
		else {
			redirect('?do=login');
		}
		break;
		case 'rss':
			echo '<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
 <title>Calendrier de l’avent</title>
 <subtitle>Une surprise par jour</subtitle>
 <link href="https://'.$_SERVER['SERVER_NAME'].'"/>
 <updated>2010-05-13T18:30:02Z</updated>
 <id>'.md5($_SERVER['SERVER_NAME']).'</id>';
			$gifts = json_decode(file_get_contents('data/data.json'), true);
			array_reverse($gifts);
			$day = date('d', time());
			if((date('m', time()) == 12) || DEV == true) {
				foreach ($gifts as $gift=>$link) {
					if($gift <= $day) {
					echo ' <entry>
				   <title>Jour #'.$gift.'</title>
				   <link href="'.$link['link'].'"/>
				   <id>'.md5($_SERVER['SERVER_NAME'].date('Y-m-d', time())).'</id>
				   <updated>'.date(DATE_ATOM, mktime(0,0,0, 12, $gift, date('Y', time()))).'</updated>
				   <summary>Voici la découverte du jour : <a href="'.$link.'">'.$link.'</a></summary>
				 </entry>';
					}
				}
			}
		echo '</feed>';
		break;
	}
}
else {
	echo show_header();
	$gifts = json_decode(file_get_contents('data/data.json'), true);
	$day = (DEV == false) ? date('d', time()) : 12;
	echo '<nav>';
	if((date('m', time()) == 12) || DEV == true) {
		foreach ($gifts as $gift=>$link) {
			if($gift < $day) {
				echo '<a href="'.$link.'" class="open">'.$gift.'</a>'.PHP_EOL;
			}
			elseif($gift == $day) {
				echo '<a href="'.$link.'" class="day">'.$gift.'</a>'.PHP_EOL;
			}
			else {
				echo '<a class="soon">'.$gift.'</a>'.PHP_EOL;
			}
		}
	}
	else {
		echo '<p style="text-align:center; color:#fff; font-size:5em;">Merci d’attendre décembre 😉</p>';
	}
	echo '</nav>';
	echo show_footer();
}
?>


