<?php
define('DEV', false);
if(!function_exists('yaml_emit_file')) {
	require_once('vendor/autoload.php');
	function yaml_parse_file($file) {
		return Symfony\Component\Yaml\Yaml::parseFile($file);
	}
	function yaml_emit_file($file, $array) {
		$yaml = Symfony\Component\Yaml\Yaml::dump($array);
		file_put_contents($file, $yaml);
	}
}
if(!isset($_GET['do'])) {$_GET['do'] = null;}
if(!file_exists('data')) {
	mkdir('data/');
}
if(!file_exists('data/'.date('Y', time()).'.yml')) {
	$gifts = array (
		1  => '',
		2  => '',
		3  => '',
		4  => '',
		5  => '',
		6  => '',
		7  => '',
		8  => '',
		9  => '',
		10 => '',
		11 => '',
		12 => '',
		13 => '',
		14 => '',
		15 => '',
		16 => '',
		17 => '',
		18 => '',
		19 => '',
		20 => '',
		21 => '',
		22 => '',
		23 => '',
		24 => '',
		25 => ''
	);
	//file_put_contents('data/data.json', json_encode($gifts));
	yaml_emit_file('data/'.date('Y', time()).'.yml', $gifts);
}
function show_header() {
	echo '<!doctype html>
	<html lang="fr">
	<head>
	<title>Calendrier de l’avent</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
	<link rel="stylesheet" media="screen" href="style.css" type="text/css"/> 
	<link rel="alternate" type="application/rss+xml" href="?do=rss" />
	</head>
	<body>
	<main>
	<h1>🎉🎊 Calendrier de l’avent 🎄✨</h1>';
}
function show_footer() {
	echo '</main>
		<footer>
		<p><a href="index.php">Calendrier</a> <a href="?do=rss">RSS</a> <a href="?do=archives">Archives</a> <a href="?do=raw">Archives brutes</a></p>
		<p>Un petit logiciel minimaliste pour faire un calendrier de l’avent maison. Le <a href="https://framagit.org/qwertygc/calendrier-avent/">code source</a> est disponible, et les icônes viennent d’<a href="https://openmoji.org">Openmoji</a>. Joyeuses fêtes.</p>
		</footer>
		</body>
		</html>';
}
##########################
if(isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'rss':
			echo '<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
 <title>Calendrier de l’avent</title>
 <subtitle>Une surprise par jour</subtitle>
 <link href="https://'.$_SERVER['SERVER_NAME'].'"/>
 <updated>2010-05-13T18:30:02Z</updated>
 <id>'.md5($_SERVER['SERVER_NAME']).'</id>';
			$gifts = yaml_parse_file('data/'.date('Y', time()).'.yml');
			array_reverse($gifts);
			$day = date('d', time());
			if((date('m', time()) == 12) || DEV == true) {
				foreach ($gifts as $gift=>$link) {
					if($gift <= $day) {
					echo ' <entry>
				   <title>Jour #'.$gift.'</title>
				   <link href="'.$link.'"/>
				   <id>'.md5($_SERVER['SERVER_NAME'].date('Y-m-d', time())).'</id>
				   <updated>'.date(DATE_ATOM, mktime(0,0,0, 12, $gift, date('Y', time()))).'</updated>
				   <summary>Voici la découverte du jour : <a href="'.$link.'">'.$link.'</a></summary>
				 </entry>';
					}
				}
			}
		echo '</feed>';
		break;
		case 'archives':
			show_header();
			$files = array_reverse(glob('data/*.{yml}', GLOB_BRACE));
			foreach($files as $file) {
				if($file != 'data/'.date('Y', time()).'.yml') {
				  echo '<h2>'.basename($file, '.yml').'</h2>';
				  $gifts = yaml_parse_file($file);
				  echo '<nav>';
					foreach ($gifts as $gift=>$link) {
						echo '<a href="'.$link.'" class="open">'.$gift.'</a>'.PHP_EOL;
					}
					echo '</nav>';
				}
			}
			show_footer();
		break;
			case 'raw':
			header("Content-Type: text/plain");
			$files = array_reverse(glob('data/*.{yml}', GLOB_BRACE));
			foreach($files as $file) {
			  echo '# '.basename($file, '.yml').PHP_EOL;
			  $gifts = yaml_parse_file($file);
				foreach ($gifts as $gift=>$link) {
					echo '- Jour #'.$gift.' : '.$link.PHP_EOL;
				}
				echo PHP_EOL;
			}
		break;
	}
}
else {
	show_header();
	$gifts = yaml_parse_file('data/'.date('Y', time()).'.yml');
	$day = (DEV == false) ? date('d', time()) : 12;
	echo '<nav>';
	if((date('m', time()) == 12) || DEV == true) {
		foreach ($gifts as $gift=>$link) {
			if($gift < $day) {
				echo '<a href="'.$link.'" class="open">'.$gift.'</a>'.PHP_EOL;
			}
			elseif($gift == $day) {
				echo '<a href="'.$link.'" class="day">'.$gift.'</a>'.PHP_EOL;
			}
			else {
				echo '<a class="soon">'.$gift.'</a>'.PHP_EOL;
			}
		}
	}
	else {
		echo '<p style="text-align:center; color:#fff; font-size:5em;">Merci d’attendre décembre 😉</p>';
	}
	echo '</nav>';
	show_footer();
	
}
?>


